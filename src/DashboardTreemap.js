import * as React from 'react'
import { ResponsiveTreeMap } from '@nivo/treemap'
import { CustomColors } from './common/variables'
import Heading from './common/Heading'

const theme = {
  radialLabels: {
    textColor: '#000',
    fontSize: '50px',
  },
}

class CustomTreemap extends React.Component {
  render() {
    return (
      <div className='container'>
        <Heading header={this.props.header} />
        <div className='visualization-wide'>
          <ResponsiveTreeMap
            root={this.props.data}
            identity='name'
            value='loc'
            innerPadding={3}
            outerPadding={3}
            margin={{
              top: 10,
              right: 10,
              bottom: 10,
              left: 10,
            }}
            label='loc'
            labelFormat='.0s'
            labelSkipSize={12}
            labelTextColor='inherit:darker(1.2)'
            colors={CustomColors}
            colorBy='depth'
            borderColor='inherit:darker(0.3)'
            animate
            motionStiffness={90}
            motionDamping={11}
          />
        </div>
      </div>
    )
  }
}

export const DashboardTreemap = CustomTreemap
