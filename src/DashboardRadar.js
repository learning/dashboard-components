import * as React from 'react'
import { ResponsiveRadar } from '@nivo/radar'
import { CustomColors } from './common/variables'
import Heading from './common/Heading'

const theme = {
  radialLabels: {
    textColor: '#000',
    fontSize: '50px',
  },
}

class CustomRadar extends React.Component {
  render() {
    return (
      <div className='container'>
        <Heading header={this.props.header} />
        <div className='visualization'>
          <ResponsiveRadar
            data={this.props.data}
            keys={this.props.keys}
            indexBy={this.props.index}
            margin={{
              top: 70,
              right: 80,
              bottom: 40,
              left: 80,
            }}
            curve='catmullRomClosed'
            borderWidth={2}
            borderColor='inherit'
            gridLevels={5}
            gridShape='circular'
            gridLabelOffset={36}
            enableDots
            dotSize={8}
            dotColor='inherit'
            dotBorderWidth={0}
            dotBorderColor='#ffffff'
            enableDotLabel
            dotLabel='value'
            dotLabelYOffset={-12}
            colors={CustomColors}
            colorBy='key'
            fillOpacity={0.1}
            animate
            motionStiffness={90}
            motionDamping={15}
            isInteractive
            legends={[
              {
                anchor: 'top-left',
                direction: 'column',
                translateX: -50,
                translateY: -40,
                itemWidth: 80,
                itemHeight: 20,
                symbolSize: 12,
                symbolShape: 'circle',
              },
            ]}
          />
        </div>
      </div>
    )
  }
}

export const DashboardRadar = CustomRadar
