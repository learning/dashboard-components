import * as React from 'react'
import { ResponsivePie } from '@nivo/pie'
import { CustomColors } from './common/variables'
import Heading from './common/Heading'

const theme = {
  radialLabels: {
    textColor: '#000',
    fontSize: '50px',
  },
}

class CustomPie extends React.Component {
  render() {
    return (
      <div className='container'>
        <Heading header={this.props.header} tooltip={this.props.tooltip} />
        <div className='visualization'>
          <ResponsivePie
            data={this.props.data}
            margin={{
              top: 40,
              right: 120,
              bottom: 40,
              left: 120,
            }}
            innerRadius={0.5}
            padAngle={0.7}
            cornerRadius={3}
            colors={CustomColors}
            colorBy='id'
            borderColor='inherit:darker(0.6)'
            radialLabelsSkipAngle={10}
            radialLabelsTextXOffset={6}
            radialLabelsTextColor='#333333'
            radialLabelsLinkOffset={0}
            radialLabelsLinkDiagonalLength={16}
            radialLabelsLinkHorizontalLength={24}
            radialLabelsLinkStrokeWidth={1}
            radialLabelsLinkColor='inherit'
            slicesLabelsSkipAngle={10}
            slicesLabelsTextColor='#fff'
            animate
            motionStiffness={90}
            motionDamping={15}
            theme={theme}
          />
        </div>
      </div>
    )
  }
}

export const DashboardPie = CustomPie
