import * as React from 'react'
import { ResponsiveBar } from '@nivo/bar'
import { BarColors } from './common/variables'
import Heading from './common/Heading'

const theme = {
  labels: {
    text: {
      fill: '#fff',
    },
  },
}

class CustomBar extends React.Component {
  render() {
    return (
      <div className='container'>
        <Heading header={this.props.header} />
        <div className='visualization-wide'>
          <ResponsiveBar
            theme={theme}
            data={this.props.data}
            keys={this.props.keys}
            indexBy={this.props.index}
            margin={
              this.props.legends !== undefined &&
              this.props.legends.length === 0
                ? {
                    top: 50,
                    right: 30,
                    bottom: 60,
                    left: 70,
                  }
                : {
                    top: 50,
                    right: 150,
                    bottom: 60,
                    left: 70,
                  }
            }
            padding={0.3}
            colors={BarColors}
            colorBy='id'
            borderColor='inherit:darker(1.6)'
            axisBottom={{
              tickSize: 5,
              tickPadding: 5,
              tickRotation: 0,
              legend: this.props.bottomLabel,
              legendPosition: 'middle',
              legendOffset: 46,
            }}
            axisLeft={{
              tickSize: 5,
              tickPadding: 5,
              tickRotation: 0,
              legend: this.props.sideLabel,
              legendPosition: 'middle',
              legendOffset: -60,
            }}
            labelSkipWidth={12}
            labelSkipHeight={12}
            labelTextColor='#ffffff'
            animate
            motionStiffness={90}
            motionDamping={15}
            legends={
              this.props.legends
                ? this.props.legends
                : [
                    {
                      dataFrom: 'keys',
                      anchor: 'bottom-right',
                      direction: 'column',
                      translateX: 120,
                      itemWidth: 100,
                      itemHeight: 20,
                      itemsSpacing: 2,
                      symbolSize: 20,
                    },
                  ]
            }
          />
        </div>
      </div>
    )
  }
}

export const DashboardBar = CustomBar
