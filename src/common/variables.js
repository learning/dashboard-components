export const CustomColors = [
  '#861F41',
  '#508590',
  '#E87722',
  '#75787B',
  '#CE0058',
  '#2CD5C4',
  '#642667',
]

export const CalendarColors = [
  '#d4f6f3',
  '#aaeee7',
  '#80e5db',
  '#56ddcf',
  '#2cd5c4',
  '#23aa9c',
  '#1a7f75',
]

export const BarColors = ['#508590', '#CE0058', '#642667']
