import React from 'react'
import './App.css'

export default class Heading extends React.Component {
  render() {
    return (
      <div className='header'>
        {this.props.header}
        {this.props.tooltip}
      </div>
    )
  }
}
