import * as React from 'react'
import Heading from './common/Heading'

class Quote extends React.Component {
  render() {
    return (
      <div className='quote-container'>
        <Heading header={this.props.header} />
        <div className='quote-viz'>
          <div className='quote-text'>{this.props.data}</div>
        </div>
      </div>
    )
  }
}

export const DashboardQuote = Quote
