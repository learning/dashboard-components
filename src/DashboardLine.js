import * as React from 'react'
import { ResponsiveLine } from '@nivo/line'
import { CustomColors } from './common/variables'
import Heading from './common/Heading'

const theme = {
  radialLabels: {
    textColor: '#000',
    fontSize: '50px',
  },
}

class CustomLine extends React.Component {
  render() {
    return (
      <div className='container'>
        <Heading header={this.props.header} />
        <div className='visualization-wide'>
          <ResponsiveLine
            data={this.props.data}
            margin={{
              top: 50,
              right: 180,
              bottom: 50,
              left: 60,
            }}
            minY='auto'
            stacked
            colors={CustomColors}
            axisBottom={{
              orient: 'bottom',
              tickSize: 5,
              tickPadding: 5,
              tickRotation: 0,
              legend: 'country code',
              legendOffset: 36,
              legendPosition: 'center',
            }}
            axisLeft={{
              orient: 'left',
              tickSize: 5,
              tickPadding: 5,
              tickRotation: 0,
              legend: 'count',
              legendOffset: -40,
              legendPosition: 'center',
            }}
            dotSize={10}
            dotColor='inherit:darker(0.3)'
            dotBorderWidth={2}
            dotBorderColor='#ffffff'
            enableDotLabel
            dotLabel='y'
            dotLabelYOffset={-12}
            animate
            motionStiffness={90}
            motionDamping={15}
            legends={[
              {
                anchor: 'bottom-right',
                direction: 'column',
                translateX: 100,
                itemWidth: 80,
                itemHeight: 20,
                symbolSize: 12,
                symbolShape: 'circle',
              },
            ]}
          />
        </div>
      </div>
    )
  }
}

export const DashboardLine = CustomLine
