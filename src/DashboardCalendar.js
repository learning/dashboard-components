import * as React from 'react'
import { ResponsiveCalendar } from '@nivo/calendar'
import { CalendarColors } from './common/variables'
import Heading from './common/Heading'

const theme = {
  radialLabels: {
    textColor: '#000',
    fontSize: '50px',
  },
}

class CustomCalendar extends React.Component {
  render() {
    return (
      <div className='container'>
        <Heading header={this.props.header} />
        <div className='visualization-cal'>
          <ResponsiveCalendar
            data={this.props.data}
            from={this.props.from}
            to={this.props.to}
            emptyColor='#fff'
            colors={CalendarColors}
            margin={{
              top: 100,
              right: 30,
              bottom: 60,
              left: 30,
            }}
            yearSpacing={100}
            minValue={1}
            direction='vertical'
            monthBorderColor='#000'
            monthBorderWidth={3}
            monthLegendOffset={15}
            dayBorderWidth={1}
            dayBorderColor='#000'
            align='center'
            legends={[
              {
                anchor: 'top-right',
                direction: 'column',
                translateY: 36,
                itemCount: 4,
                itemWidth: 34,
                itemHeight: 36,
                itemDirection: 'right-to-left',
              },
            ]}
          />
        </div>
      </div>
    )
  }
}

export const DashboardCalendar = CustomCalendar
