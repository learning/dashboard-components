import * as React from 'react'

class Number extends React.Component {
  render() {
    return (
      <div className='number-container'>
        <div className='number-viz'>
          <h2 className='number'>{this.props.data}</h2>
        </div>
        <div className='subtext'>{this.props.header}</div>
      </div>
    )
  }
}

export const DashboardNumber = Number
